#!/usr/bin/env python
# Python 3.9
# Created: 02/09/2021
__author__ = """Jacob Betz"""
__email__ = 'jake@baruek.net'

import flawlys

##############
# WSGI entry #
##############

handler = flawlys.create_app()

if __name__ == '__main__':
    # Entry point when run via Python interpreter
    print('== Running in debug mode ==')
    flawlys.create_app().run(host='0.0.0.0', port=8080, debug=True)
