''' Application (flawlys) entry point '''
import os
from flask import Flask


def _initialize_blueprints(flawlys) -> None:
    ''' Register Flask blueprints '''
    #from flawlys.routes.message import message


def create_app() -> Flask:
    ''' Create an app by initializing components '''
    flawlys = Flask(__name__)
    _initialize_blueprints(flawlys)
    return flawlys
