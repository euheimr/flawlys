#!/usr/bin/env python
# Python 3.9
# Created: 02/09/2021
__author__ = """Jacob Betz"""
__email__ = 'jake@baruek.net'


class Config(object):
    DEBUG = False
    TESTING = False

    DATABASE_URL = 'postgres://postgres:@postgres:5432/'


class ProductionConfig(Config):
    DATABASE_URL = 'redis://user@localhost/foo'


class DevelopmentConfig(Config):
    DB_SERVER = 'localhost'
    DEBUG = True


class TestingConfig(Config):
    DB_SERVER = 'localhost'
    TESTING = True
