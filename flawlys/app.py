#!/usr/bin/env python
# Python 3.9
# Created: 02/09/2021
#   ______
#  / ____/            |\
# | |_| | __ _._      | |_  __ ____
# |  _| |/  ` | \ /\ /| | | | / ___\
# | | | | (|  |\ V  V | | \_| \___ \
# | | | |\__,_/ \_/\_/| |\__  \____/
# |/   \'             '/ \___/
__author__ = '''Jacob Betz'''
__email__ = 'jake@baruek.net'


from flask import Flask
from config import ProductionConfig, DevelopmentConfig

app = Flask(__name__)


@app.route('/')
def main():
    # Set the default settings
    app.config.from_object(config.DevelopmentConfig())

    return render_template('index.html')


if __name__ == '__main__':
    app.run()
