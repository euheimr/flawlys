** Warning: Early work in progress. Features are not guaranteed to work until the project reaches version 1.0.0 **

`flawlys` is a Python Security Analytics Dashboard on [Flask](https://flask.palletsprojects.com/) using [Dash by Plotly](https://plotly.com/dash/) on [React](https://reactjs.org/) deployed to a docker container to perform Security analysis utilizing the Amazon Web Services API and Qualys APIv2.


# Table of Contents
- [Overview](#overview)
- [Requirements](#requirements)
    - [Install](#install)
- [Development](#development)
- [Development Resources](#development-resources)
    - [Flask](#flask)
        - [Dash by Plotly](#dash)
    - [AWS](#aws)
    - [Qualys API](#qualys-api)
    - [Books](#books)
    - [Serverless](#serverless)


## Overview
The goal of `flawlys` is to provide a reliable, cost-effective multi-purpose analytics dashboarding tool for enterprises using Amazon Web Services and Qualys.

Analytics and data parsing:
1. Is functionally & imperatively written to avoid unexpected object states, as opposed to Object-Oriented (OOP)
    - OOP can naturally introduce unexpected application behavior that is hard to test for or at worst, corrupt analytical data
    - Additionally, this fundamental design decision will help make development, testing and deployment of `flawlys` straight-forward and serverless-friendly (where object states are not guaranteed across function calls)
2. Operates on the principle that risking flaws in data integrity for `flawlys` could potentionally create unnecessary work for multiple parties in operational environments due to erroneous data
    - Data integrity should be trusted and paramount where integrity is important
3. When datasets are created, they are never modified. Rather, changed datasets employ a new object. If objects/datasets require modification then new objects are created:

            dataset = [0, 'aws', 'qualys']
            new_dataset = dataset[1].rstrip('s')

4. Uses `black` as the code formatter

5. Uses [pyqlsapi](https://gitlab.com/euheimr/pyqlsapi) - a third-party functionally-written Python API wrapper for Qualys APIv2 written by me.
    - this library _only_ uses HTTPS session-based authentication to Qualys (as opposed to Qualys APIv1 (HTTP))

## Requirements
* Python>= 3.9 - [[download](https://www.python.org/downloads/release/python-381/)]
* AWS API - boto3 (Python API wrapper) [[docs](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)]
    * READ-ONLY role access
* Flask (web application) - [[docs]()]
* requests () - [[docs]()]
* pyqlsapi (Python Qualys API Wrapper) - [[main page](https://gitlab.com/euheimr/pyqlsapi)]

### Install
1. Copy the Repository
2. Open a command prompt with the working directory being this copied project directory

    `cd <PROJECT_PATH>`

3. Create a new environment
 named `venv` and activate it:

    **MacOS/Linux**

        virtualenv venv
        . /venv/bin/activate && pip install -r requirements.txt
        export FLASK_APP=flawlys && export FLASK_ENV=development
        pip install -e .
        flask run

    **Windows**

        py -3 -m venv venv & venv\Scripts\activate
        pip install -r requirements.txt
        set FLASK_APP=flawlys
        set FLASK_ENV=development
        pip install -e .
        flask run

4. Check to ensure AWS CLI is installed:

    `aws --version`

5. Check to ensure Node.js is installed:

    `npm --version`


## Development TODO


## Development Resources
To work on and help complete this project, I've referred to the following resources:
* [Links List] [Awesome Python](https://awesome-python.com/)
* [Post Collection] [Python Awesome - Data Visualization](https://pythonawesome.com/tag/data-visualization/)

### Flask TODO
*

#### Dash
* [medium.com] [Embed a Dash app into an existing Flask app](https://medium.com/@olegkomarov_77860/how-to-embed-a-dash-app-into-an-existing-flask-app-ea05d7a2210b)

### AWS
* [Learn AWS Serverless Computing: A beginner's guide to using AWS Lambda, Amazon API Gateway, and services from Amazon Web Services](https://www.amazon.com/gp/product/1789958350/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
* [AWS Security Cookbook: Practical solutions for managing security policies, monitoring, auditing, and compliance with AWS](https://www.amazon.com/gp/product/1838826254/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)


### Qualys API
* [PDF] [Qualys API Limits](https://www.qualys.com/docs/qualys-api-limits.pdf)
* [PDF] [Qualys API - Tracking API Usage](https://www.qualys.com/docs/qualys-api-tracking-api-usage.pdf)
* [Qualys Documentation Page](https://www.qualys.com/documentation/)
    * [PDF] [Qualys API Quick Reference](https://www.qualys.com/docs/qualys-api-quick-reference.pdf)
    * [PDF] [Qualys API (VM,PC) User Guide](https://www.qualys.com/docs/qualys-api-vmpc-user-guide.pdf)
    * [PDF] [Qualys API (VM,PC) XML/DTD Reference](https://www.qualys.com/docs/qualys-api-vmpc-xml-dtd-reference.pdf)
    * [PDF] [Qualys API - Asset Management & Tagging v2 API](https://www.qualys.com/docs/qualys-asset-management-tagging-api-v2-user-guide.pdf)
    * [PDF] [Qualys API - Cloud Agent API User Guide](https://www.qualys.com/docs/qualys-ca-api-user-guide.pdf)
    * [PDF] [Qualys API - CloudView API User Guide](https://www.qualys.com/docs/qualys-cloudview-api-user-guide.pdf)
    * [PDF] [Web Application Scanning API - User Guide](https://www.qualys.com/docs/qualys-was-api-user-guide.pdf)
    * [PDF] [Web Application Firewall API - User Guide](https://www.qualys.com/docs/qualys-waf-api-user-guide.pdf)


### Books
* [Python 2.7 & 3.4 Pocket Reference (O'Reilly)](https://www.amazon.com/gp/product/1449357016/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
* [Regular Expression Pocket Reference: Regular Expressions For Perl, Ruby, Php, Python, C, Java And .Net (Pocket Reference (O'Reilly)](https://www.amazon.com/gp/product/0596514271/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
* [Python for Data Analysis: Data Wrangling with Pandas, NumPy, and IPython](https://www.amazon.com/gp/product/1491957662/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
* [Python for DevOps: Learn Ruthlessly Effective Automation](https://www.amazon.com/gp/product/149205769X/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
* [High Performance Python: Practical Performant Programming for Humans Practical Performant Programming for Humans](https://www.amazon.com/gp/product/1449361595/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
* [Fluent Python: Clear, Concise, and Effective Programming](https://www.amazon.com/gp/product/1491946008/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)


### Serverless
* [Serverless WSGI](https://www.serverless.com/plugins/serverless-wsgi)
* [Serverless Offline](https://github.com/dherault/serverless-offline)
* [Serverless Offline SNS](https://www.serverless.com/plugins/serverless-offline-sns)
* [Serverless Python Requirements](https://github.com/UnitedIncome/serverless-python-requirements)
* [Serverless Plugin Include Dependencies](https://www.serverless.com/plugins/serverless-plugin-include-dependencies)
* [Serverless Plugin AWS Alerts](https://www.serverless.com/plugins/serverless-plugin-aws-alerts)



