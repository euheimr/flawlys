#!/usr/bin/env python
# Python 3.9
# Created: 02/09/2021
__author__ = """Jacob Betz"""
__email__ = 'jake@baruek.net'

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='flawlys',
    version='0.0.1',
    author='Jacob Betz',
    author_email='jake@baruek.net',
    description='A Serverless Analytics Dashboard for AWS Cloud and Qualys.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/remidra/flawlys',
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    install_requires=[
        'flask',
        'boto3',
        'requests',
    ]
)
